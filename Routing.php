<?php

require_once 'src/controllers/DefaultController.php';
require_once 'src/controllers/SecurityController.php';
require_once 'src/controllers/ScheduleController.php';

class Router
{

    public static $routes;

    public static function get($url, $view)
    {
        self::$routes[$url] = $view;
    }

    public static function post($url, $view)
    {
        self::$routes[$url] = $view;
    }



    public static function run($url)
    {
        $sessionControl = new SessionController();
        $securityControl = new SecurityController();
        if ($url === 'register'){
            $url= "register";
        }
        elseif ($url === 'add_schedule'){
           if(($securityControl->returnUserData()['user_type']) == 2){
               $url= "add_schedule";
           }
           else{
               $url= "schedule";
           };

        }
        else {
            if (!$sessionControl->checkCookieWithDatabase()) {
                $url = "login";
            } elseif ($url === 'login') {
                $url = '';
            }
        }

        $action = explode("/", $url)[0];
        if (!array_key_exists($action, self::$routes)) {
            die("Wrong url!");
        }

        $controller = self::$routes[$action];
        $object = new $controller;
        $action = $action ?: 'index';

        $object->$action();
    }


}