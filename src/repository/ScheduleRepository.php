<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/Schedule.php';

class ScheduleRepository extends Repository
{
    public function getUserFromCookieWithDatabase() : int{
        $cookie = json_decode($_COOKIE['user'], true);

        $stmt = $this->database->connect()->prepare('
            SELECT uc.class_id FROM public.users u join users_class uc on u.id = uc.user_id WHERE user_email = :email;
        ');
        $stmt->bindParam(':email', $cookie['email'], PDO::PARAM_STR);
        $stmt->execute();

        $respond = $stmt->fetch(PDO::FETCH_ASSOC);
        return $respond;

    }



    public function getUserByCookie(): int
    {
        $cookie = json_decode($_COOKIE['user'], true);

        $stmt = $this->database->connect()->prepare('
            SELECT class_id FROM public.users u join users_class uc on u.id = uc.user_id WHERE user_email = :email
        ');
        $stmt->bindParam(':email', $cookie['email'], PDO::PARAM_STR);
        $stmt->execute();

        $respond = $stmt->fetch(PDO::FETCH_ASSOC);
        return $respond["class_id"];

    }

    public function mapDay(string $day): int
    {
        $stmt = $this->database->connect()->prepare("
               select id from classes_days where day='$day';
        ");
        $stmt->execute();
        $classes_day_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $classes_day_arr[0]['id'];
    }

    public function mapPeriod(string $period): int
    {
        $stmt = $this->database->connect()->prepare("
               select id from classes_periods where period='$period';
        ");
        $stmt->execute();
        $classes_periods_arr = $stmt->fetch(PDO::FETCH_ASSOC);
        return $classes_periods_arr['id'];
    }


    public function mapClass(string $class): int
    {
        $stmt = $this->database->connect()->prepare("
               select id from class_names where name='$class';
        ");
        $stmt->execute();
        $class_names_arr = $stmt->fetch(PDO::FETCH_ASSOC);
        return $class_names_arr['id'];
    }


    public function mapClassId(int $class_id): string
    {
        $stmt = $this->database->connect()->prepare("
               select name as class_name from class_names where id='$class_id';
        ");
        $stmt->execute();
        $class_names_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $class_names_arr[0]['class_name'];
    }

    public function mapSubject(string $subject): int
    {
        $stmt = $this->database->connect()->prepare("
               select id from subjects where name='$subject';
        ");
        $stmt->execute();
        $subjects_arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $subjects_arr[0]['id'];
    }


    public function addSchedule(Schedule $schedule): void
    {

        $stmt = $this->database->connect()->prepare("
            
                INSERT INTO schedule_data (period_id, class_id, subject_id, classes_day_id) VALUES (?,?,?,?)
            
        ");
        $stmt->execute([
            $this->mapPeriod($schedule->getPeriod()),
            $this->mapClass($schedule->getClassName()),
            $this->mapSubject($schedule->getClassesName()),
            $this->mapDay($schedule->getSchoolDay())

        ]);

    }



    public function getSchedules(?string $day = NULL): array
    {

        if ($day == NULL) {
            $classes_day = '1';

        } else {
            $classes_day = $this->mapDay($day);

        }
        $class_id = $this->getUserByCookie();
        $result = [];
        $stmt = $this->database->connect()->prepare("
               select cp.period as period, cd.day as school_day , s.name as classes_name, cn.name as class_name from schedule_data sd
                   join subjects s on sd.subject_id = s.id
                join class_names cn on cn.id = sd.class_id
                join classes_days cd on cd.id = sd.classes_day_id
                join classes_periods cp on sd.period_id = cp.id
                where sd.classes_day_id='$classes_day' AND sd.class_id = '$class_id' order by sd.period_id asc;
        ");
        $stmt->execute();
        $schedules = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($schedules as $schedule) {

            $result[] = new Schedule(
                $schedule['period'], $schedule['school_day'], $schedule['class_name'], $schedule['classes_name']
            );
        }

        return $result;
    }




}