<?php

require_once 'Repository.php';
require_once __DIR__ . '/../models/User.php';

class UserRepository extends Repository
{

    public function getUser(string $email): ?User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users u join users_class uc on uc.user_id = u.id JOIN public.credentials c on c.user_id = u.id WHERE u.user_email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user == false) {
            return null;
        }
        return new User($user['user_email'], $user['user_password'], $user['user_firstname'], $user['user_lastname'], $user['class_id']

        );
    }

    public function addUser(User $user)
    {
        $pdo = $this->database->connect();
        $stmt = $this->database->connect()->prepare('
            INSERT INTO users (user_firstname, user_lastname, user_email)
            VALUES (?, ?, ?)
        ');

        $pdo->beginTransaction();

        try {
            $stmt->execute([
                $user->getName(),
                $user->getSurname(),
                $user->getEmail(),

            ]);

            $pdo->commit();
        } catch (Exception $e) {
            $pdo->rollBack();
        }

        $stmt = $this->database->connect()->prepare('
            INSERT INTO credentials (user_password, user_id)
            VALUES (?, ?)
        ');

        $stmt->execute([
            $user->getPassword(),
            $this->getUserId($user)
        ]);


        $stmt = $this->database->connect()->prepare('
            INSERT INTO users_class (user_id, class_id)
            VALUES (?, ?)
        ');

        $stmt->execute([
            $this->getUserId($user),
            $user->getClassId()
        ]);


    }

    public function getUserId(User $user): int
    {
        $stmt = $this->database->connect()->prepare('
            SELECT u.id FROM public.users u WHERE user_firstname = :name AND user_lastname = :surname AND user_email = :email
        ');
        $stmt->bindParam(':name', $user->getName(), PDO::PARAM_STR);
        $stmt->bindParam(':surname', $user->getSurname(), PDO::PARAM_STR);
        $stmt->bindParam(':email', $user->getEmail(), PDO::PARAM_STR);

        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['id'];
    }

    public function getUserClassName(User $user): string
    {
        $stmt = $this->database->connect()->prepare('
            SELECT cn.name as class_name FROM public.users u join users_class uc on uc.user_id=u.id join class_names cn on cn.id=uc.class_id WHERE user_firstname = :name AND user_lastname = :surname AND user_email = :email
        ');
        $stmt->bindParam(':name', $user->getName(), PDO::PARAM_STR);
        $stmt->bindParam(':surname', $user->getSurname(), PDO::PARAM_STR);
        $stmt->bindParam(':email', $user->getEmail(), PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data['class_name'];
    }

    public function getUserData(): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT u.user_firstname, u.user_lastname, cn.name as class_name, u.type_id as user_type FROM users u join users_class uc on u.id = uc.user_id join class_names cn on uc.class_id = cn.id where user_email=:email
        ');
        $stmt->bindParam(':email', json_decode($_COOKIE["user"], true)["email"], PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($data == false) {
            return [];
        }
        return $data;
    }


}




