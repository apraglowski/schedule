<?php


class Schedule
{
    private $period;
    private $school_day;
    private $class_name;
    private $classes_name;


    public function __construct($period, $school_day, $class_name, $classes_name)
    {
        $this->period = $period;
        $this->school_day = $school_day;
        $this->class_name = $class_name;
        $this->classes_name = $classes_name;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function setPeriod($period): void
    {
        $this->period = $period;
    }

    public function getSchoolDay()
    {
        return $this->school_day;
    }

    public function setSchoolDay($school_day): void
    {
        $this->school_day = $school_day;
    }

    public function getClassName()
    {
        return $this->class_name;
    }

    public function setClassName($class_name): void
    {
        $this->class_name = $class_name;
    }

    public function getClassesName()
    {
        return $this->classes_name;
    }

    public function setClassesName($classes_name): void
    {
        $this->classes_name = $classes_name;
    }





}