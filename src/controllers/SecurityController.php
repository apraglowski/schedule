<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../repository/UserRepository.php';
require_once __DIR__ . '/../repository/ScheduleRepository.php';

class SecurityController extends AppController
{
    private $userRepository;
    private $scheduleRepository;


    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
        $this->scheduleRepository = new ScheduleRepository();

    }

    public function login()
    {
        $this->returnUserData();
        $userRepository= new UserRepository();
        $sessionControl = new SessionController();
        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $user= $userRepository->getUser($email);

        if(!$user){
            return $this->render('login', ['messages' => ['User not exist!']]);
        }

        if ($user->getEmail() !== $email) {
            return $this->render('login', ['messages' => ['User with this email not exist!']]);
        }

        if ($user->getPassword() !== $password) {
            return $this->render('login', ['messages' => ['Wrong password!']]);
        }
        $sessionControl->setCookie($user->getEmail());

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/account");
    }
    public function logout(){
        $sessionControl = new SessionController();
        $sessionControl->flushUserCoockie();
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}/");
    }
    public function account(): void
    {

        $temp = $this->userRepository->getUserData();
        $this->render('account', ['user_name' => $temp["user_firstname"], 'user_surname' => $temp["user_lastname"], 'class_name' => $temp["class_name"]]);
    }



    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        $class_name = $_POST['class_name'];
        $class_id = $this->scheduleRepository->mapClass($class_name);
        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }
        $user = new User($email, md5($password), $name, $surname, $class_id);
        $this->userRepository->addUser($user);
        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }


     public function returnUserData() : array {
         return $this->userRepository->getUserData();
     }


}