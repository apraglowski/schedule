<?php

require_once 'AppController.php';
require_once __DIR__ . '/../models/Schedule.php';
require_once __DIR__ . '/../repository/ScheduleRepository.php';
require_once __DIR__ . '/../repository/UserRepository.php';

class ScheduleController extends AppController
{

    private $message = [];
    private $scheduleRepository;
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->scheduleRepository = new ScheduleRepository();
        $this->userRepository = new UserRepository();
    }

    public function addSchedule()
    {
        if($this->isPost()){
            $temp = $this->userRepository->getUserData();
            $schedule = new Schedule($_POST['period'], $_POST['school_day'], $_POST['class_name'], $_POST['classes_name']);
            $this->scheduleRepository->addSchedule($schedule);
            return $this->render('schedule', [
                'messages' => $this->message,
                'schedule' => $this->scheduleRepository->getSchedules()
            ]);
        }
        return $this->render('add_schedule', ['messages' => $this->message, 'user_type' => $temp["user_type"]], );
    }




    public function schedule()
    {
        if (isset($_POST['school_day'])) {
            $schedule = $this->scheduleRepository->getSchedules($_POST['school_day']);
        }
        else {
            $schedule = $this->scheduleRepository->getSchedules();
        }
        $this->render('schedule', ['schedule' => $schedule]);
    }


    public function add_schedule()
    {
        $this->render('add_schedule');
    }





}

