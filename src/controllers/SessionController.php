<?php
require_once __DIR__ . '/../../Database.php';
require_once __DIR__ . '/../repository/SessionRepository.php';


class SessionController
{
    protected $sessionRepository;
    public function __construct()
    {
        $this->sessionRepository = new SessionRepository();

    }


    public function setCookie(string $coockie_email)
    {
       $this->sessionRepository->cookieSetter($coockie_email);

    }

    public function checkCookieWithDatabase()
    {
        return $this->sessionRepository->checkCookieRepo();
    }


    public function flushUserCoockie()
    {
      $this->sessionRepository->deleteCoockieInDatabase();
    }




}
