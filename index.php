<?php

require 'Routing.php';
require  __DIR__.'/src/controllers/SessionController.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('account', 'SecurityController');
Router::get('register', 'DefaultController');
Router::get('schedule', 'ScheduleController');
Router::get('add_schedule', 'ScheduleController');
Router::post('addSchedule', 'ScheduleController');
Router::get('logout', 'SecurityController');
Router::post('schedule', 'ScheduleController');
Router::post('login', 'SecurityController');
Router::post('register', 'SecurityController');

Router::run($path);







