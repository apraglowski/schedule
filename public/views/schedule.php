<!DOCTYPE html>
<head>
    <title>Account Page</title>
    <link rel="stylesheet" type="text/css" href="public/css/schedule.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">

    <script src="https://kit.fontawesome.com/ef8bc7d9b8.js" crossorigin="anonymous"></script>

</head>
<body>

<div class="base-container">
    <div id="main-page">
        <div id="header">
            <img src="public/img/logo.svg">
            <div id="header2">
                <header class="upper">

                    <div>
                        <form action="logout" method="POST">
                            <button class="button-top" type="submit">Logout</button>
                        </form>
                    </div>
                    <div>
                        <button class="button-top" type="submit">Account</button>
                    </div>
                </header>


            </div>
        </div>
        <main>
            <nav>
                <ul>
                    <li>
                        <i class="fas fa-user"></i>
                        <a href="/account" class="button">account</a>
                    </li>
                    <li>
                        <i class="far fa-clock"></i>
                        <a href="/schedule" class="button">schedule</a>
                    </li>
                    <li>
                        <i class="fas fa-bell"></i>
                        <a href="/add_schedule" class="button">add</a>
                    </li>
                </ul>
            </nav>
            <section class="schedules-container">
                <div class="sc-container">
                    <div>
                        <form class="schedule-form-day" action="schedule" method="POST" ENCTYPE="multipart/form-data">
                            <?php if (isset($messages)) {
                                foreach ($messages as $message)
                                    echo $message;
                            }
                            ?>
                            <label for="browser">Choose day:</label>
                            <input list="schedule_school_day" name="school_day">
                            <datalist id="schedule_school_day">
                                <option value=Monday>
                                <option value=Tuesday>
                                <option value=Wednesday>
                                <option value=Thursday>
                                <option value=Friday>
                            </datalist>
                            <button type="Submit">send</button>
                        </form>
                    </div>
                    <div style="display: table;">
                        <table class="schedule-table">
                            <thead>
                            <tr>
                                <th colspan="2">
                                <?php if(isset($_POST["school_day"])){echo ($_POST["school_day"]);}else {echo "Monday";} echo ("<br>");?>
                                </th>
                            </tr>
                            <tr>
                                <th><?php  echo "Period" ?></th>
                                <th><?php  echo "Class Name" ?></th>
                            </tr>
                            </thead>

                            <tbody>
                        <?php foreach ($schedule as $sched): ?>

                            <tr>
                                <td><?php   echo $sched->getPeriod()?></td>
                                <td><?php echo  $sched->getClassesName()?></td>
                            </tr>
                        <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
            </section>
        </main>
    </div>
    <footer>
        <div>
            Lorem ipsum
        </div>
    </footer>
</div>