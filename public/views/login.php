<!DOCTYPE html>
<head>
    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
</head>
<body>

<div class="container">
    <div class="logo">
        <img src="public/img/logo.svg">
    </div>
    <div class="login-container">
        <form class="login" action="login" method="POST">
            <div class="messages" >
                <?php if (isset($messages)) {
                    foreach ($messages as $message)
                        echo $message;
                }
                ?>
            </div>
            <input name="email" type="text" placeholder="email@domain.com">
            <input name="password" type="password" placeholder="password">
            <button type="submit">login</button>
        </form>
        <a href="/register">You don't have account? Register!</a>

    </div>


</div>

</body>

