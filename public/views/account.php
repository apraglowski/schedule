<!DOCTYPE html>
<head>
    <title>Account Page</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/account.css">
    <script src="https://kit.fontawesome.com/ef8bc7d9b8.js" crossorigin="anonymous"></script>

</head>
<body>

<div class="base-container">
    <div id="main-page">
        <div id="header">
            <img src="public/img/logo.svg">
            <div id="header2">
                <header class="upper">

                    <div>
                        <form action="logout" method="POST">
                            <button class="button-top" type="submit">Logout</button>
                        </form>
                    </div>
                    <div>
                        <button class="button-top" type="submit"><?php echo $user_name?></button>
                    </div>
                </header>
            </div>
        </div>
        <main>
            <nav>
                <ul>
                    <li>
                        <i class="fas fa-user"></i>
                        <a href="/account" class="button">account</a>
                    </li>
                    <li>
                        <i class="far fa-clock"></i>
                        <a href="/schedule" class="button">schedule</a>
                    </li>
                    <li>
                        <i class="fas fa-bell"></i>
                        <a href="/add_schedule" class="button">add</a>
                    </li>
                </ul>
            </nav>
            <section class="store-container">
                <div class="left-user-account">
                    <img src="public/img/logo.svg">
                </div>
                <div class="right-user-account">
                    <div class="user-account"><?php echo $user_name?> </div>
                    <div class="user-account"><?php echo $user_surname?></div>
                    <div class="user-account"><?php echo $class_name?></div>
                    <a href="/add_schedule"><div class="user-schedule-button">schedule</div></a>

                </div>


            </section>
        </main>
    </div>
    <footer>
        <div>
            Lorem ipsum
        </div>
    </footer>
</div>