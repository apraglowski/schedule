<!DOCTYPE html>
<head>
    <title>Account Page</title>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/schedule.css">

    <script src="https://kit.fontawesome.com/ef8bc7d9b8.js" crossorigin="anonymous"></script>

</head>
<body>

<div class="base-container">
    <div id="main-page">
    <div id="header">
        <img src="public/img/logo.svg">
        <div id="header2">
            <header class="upper">

                <div>
                    <form action="logout" method="POST">
                        <button class="button-top" type="submit">Logout</button>
                    </form>
                </div>
            </header>
            <header class="lower">
                <div>
                </div>
            </header>
        </div>
    </div>
    <main>
        <nav>
            <ul>
                <li>
                    <i class="fas fa-user"></i>
                    <a href="/account" class="button">account</a>
                </li>
                <li>
                    <i class="far fa-clock"></i>
                    <a href="/schedule" class="button">schedule</a>
                </li>
                <li>
                    <i class="fas fa-bell"></i>
                    <a href="/add_schedule" class="button">add</a>
                </li>
            </ul>
        </nav>
        <section class="store-container">
            <form class="schedule-form" action="addSchedule" method="POST" ENCTYPE="multipart/form-data">
                <?php if (isset($messages)) {
                    foreach ($messages as $message)
                        echo $message;
                }
                ?>
                <label for="browser">Period:</label>
                <input list="period" name="period">
                <datalist id="period">
                    <option value=08:00-08:45>
                    <option value=09:00-09:45>
                    <option value=10:00-10:45>
                    <option value=11:00-11:45>
                    <option value=12:00-12:45>
                    <option value=13:00-13:45>
                    <option value=14:00-14:45>
                    <option value=15:00-15:45>
                </datalist>

                <label for="browser">Class:</label>
                <input list="class_name" name="class_name">
                <datalist id="class_name">
                    <option value=1A>
                    <option value=2A>
                    <option value=3A>
                    <option value=1B>
                    <option value=2B>
                    <option value=3B>
                    <option value=1C>
                    <option value=2C>
                    <option value=3C>
                </datalist>

                <label for="browser">Subject:</label>
                <input list="classes_name" name="classes_name">
                <datalist id="classes_name">
                    <option value=Algebra>
                    <option value=Geometry>
                    <option value=Statistics>
                    <option value=Biology>
                    <option value=Chemistry>
                    <option value=Physics>
                    <option value=Economics>
                    <option value=Geography>
                    <option value=History>
                    <option value=English>
                    <option value=PE>

                </datalist>


                <label for="browser">Day of Week:</label>
                <input list="school_day" name="school_day">
                <datalist id="school_day">
                    <option value=Monday>
                    <option value=Tuesday>
                    <option value=Wednesday>
                    <option value=Thursday>
                    <option value=Friday>
                </datalist>

                <button type="submit">send</button>
            </form>



        </section>
    </main>
</div>
<footer>
    <div>
        Lorem ipsum
    </div>
</footer>
</div>
</body>